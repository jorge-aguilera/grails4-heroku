FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim
MAINTAINER John Doe "john.doe@example.com"

WORKDIR /app
COPY build/libs/*.jar application.jar

CMD ["sh", "-c","java -Djava.security.egd=file:/dev/./urandom -jar /app/application.jar --server.port=$PORT"]
