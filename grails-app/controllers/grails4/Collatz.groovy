package grails4

class Collatz {

    BigInteger origin
    List<BigInteger> steps
    int odd
    int even

    void setOrigin(BigInteger number) {
        origin = number
        if (number < 1) {
            throw new ArithmeticException()
        }
        steps = []
        while (number != 1) {
            if (number % 2 == 0) {
                number = number / 2
                even++
            } else {
                number = number * 3 + 1
                odd++
            }
            steps << number
        }
    }
}
