<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>
</head>
<body>
<content tag="nav">

</content>


<div id="content" role="main">
    <section class="row colset-2-its">
        <h1>Welcome to Collatz Grails</h1>
    </section>

    <section class="row colset-2-its">
        <g:form controller="collatz" action="index" method="post">
            <div class="form-row col-sm-10">
                <label for="collatz">A big number</label>
                <input type="number" class="form-control" id="collatz" name="collatz" aria-describedby="collatzHelp"
                    placeholder="123456"
                    pattern="([0-9])+"
                    maxlength="100"
                    value="${result ? result.origin : '123123123123212867867867754764575671231231231234543543657767'}"                >
                <small id="collatzHelp" class="form-text text-muted">A really really big number.</small>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <g:if test="${result}">
                <div class="form-row">
                    <div class="col">
                        <label for="stepsSize">Steps</label>
                        <input type="text" id="stepsSize" class="form-control" value="${result.steps.size()}">
                    </div>
                    <div class="col">
                        <label for="odd">Oddd</label>
                        <input type="text"  id="odd" class="form-control" value="${result.odd}">
                    </div>
                    <div class="col">
                        <label for="even">Even</label>
                        <input type="text"  id="even" class="form-control" value="${result.even}">
                    </div>
                </div>
                <div class="form-group">
                    <ul class="list-group">
                        <g:each in="${result.steps}" status="i" var="step">
                            <li class="list-group-item list-group-item-${step % 2 == 0 ?'primary':'secondary'}">${i+1}.- ${step}</li>
                        </g:each>
                    </ul>
                </div>
            </g:if>
        </g:form>

    </section>

</div>

</body>
</html>
